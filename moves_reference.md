# Moves Reference
Moves can define actions and modifiers. Modifiers passively affect characters
knowing the move. Actions get used when the move is used.

Moves can also define what is visible when they're linked to chat.
Possibilities include:
* all
  * Everything goes to chat when the move is used
* attack
  * Only rolls go to chat
  * Name of the move is placed after last roll

## Starting Moves
Classes get an array of starting moves. They may be moves, or arrays of moves.
Array entries are choices between their contents.

## Actions
For all actions, specify gm: true to target GM instead of player.

* roll {roll: Roll, label: String, onclick: Map<Action, Object>}
  * Does a roll below description of this move in chat
  * Optionally, places a label under the roll, indicating what was rolled
  * Optionally, executes more actions when label is clicked
    * If there is no label, uses name of move
* label {label: String, onclick: Map<Action, Object>}
  * Displays a label
  * Optionally, when the label is clicked, execute more actions

## Modifiers
* move {class: Class, choices: Array<String>}
  * Grants access to a move
  * If the class is specified, move taken must be from that class
  * If choices are given, well, one of them must be picked
* forward {when: Move, stat: String, mod: Roll}
  * Adds a modifier to a stat next time it will be used
  * If a move is given, applies only during it
* tags {target: Array<String>, tags: Array<String>}
  * Adds tags to targets
  * Possible targets: weapon, armor, character
