
// Implements system specific
class DwActor extends Actor {
  // TODO
}

class DwActorSheet extends ActorSheet {

  get template() {
    return "public/systems/dungeonworld/templates/actor-sheet.html";
  }

  getData() {
    const sheetData = super.getData();

    // Return data to the sheet
    return sheetData;
  }

  activateListeners(html) {
    super.activateListeners(html);

    // Activate tabs
    html.find('.tabs').each((_, el) => {
      let tabs = $(el),
        initial = "overview";
      new Tabs(tabs, initial);
    });

    // Universal remove buttons
    html.find(".remove-button").click(ev => {
      const button = $(ev.currentTarget);
      const id = parseInt(button.attr("remove-id"));
      const arrayPath = button.attr("remove-array");
      let array = super.getData();
      for (const part of arrayPath.split(".")) {
        array = array[part];
      }

      new Dialog({
        title: button.attr("remove-title"),
        content: button.attr("remove-content"),
        buttons: {
          requires: {
            label: "Yes",
            callback: () => {
              array.splice(id, 1);
              this.actor.update({[arrayPath]: array}, true);
            }
          },
          replaces: {
            label: "No",
            callback: () => {
              // Do nothing, user cancelled
            }
          }
        }}).render(true);
    });
  }

  /**
   * Bring something from a compendium.
   * @param collection {String}     The name of the pack from which to import
   * @param entryId {String}        The ID of the compendium entry to import
   *
   * @return {Promise}              A Promise containing the imported entity
   */
  _getFromCompendium(collection, entryId) {
    const pack = game.packs.find(p => p.collection === collection);

    return pack.getEntity(entryId).then(ent => {
      delete ent.data._id;
      return ent.constructor.create(ent.data, true);
    });
  }

  /**
   * Handle dropped data on the Actor sheet
   * @private
   */
  _onDrop(event) {
    event.preventDefault();
    const data = JSON.parse(event.dataTransfer.getData('text/plain'));
    let entry;
    if (data.pack) { // From compendium
      entry = this._getFromCompendium(data.pack, data.id).data;
    } else if ( data.type === "Item" ) { // From world
      entry = new Promise((resolve, reject) => {
        resolve(game.items.get(data.id).data);
      });
    }

    // Actually handle the data
    entry.then((entity) => {
      if (entity.type == "class") {
        this.importClass(entity, data.pack);
      } else if (entity.type == "move") {
        this.importMove(entity, data.pack);
      }
    });
  }

  /**
   * Imports a class to this actor sheet.
   */
  importClass(data, pack) {
    const ourData = super.getData();
    if (data.data.class.value !== undefined) {
      // Existing class found, ask user what to do
      new Dialog({
        title: "Existing Class Found",
        content: `This character already has a class. Do you want to overwrite it?`,
        buttons: {
          requires: {
            label: "Yes",
            callback: () => {

            }
          },
          replaces: {
            label: "No",
            callback: () => {

            }
          }
      }});
    }

    // Copy basic class details
    const changes = {
      ["data.class.value"]: data.name,
      ["data.class.id"]: data._id,
      ["data.class.pack"]: pack,
      ["data.hp.value"]: data.maxHp.value,
      ["data.hp.max"]: data.maxHp.value,
      ["data.damage.value"]: data.baseDamage.value,
      ["data.class.value"]: data // Reference to class, for picking stuff
    };

    // Add starting moves
    const moves = ourData.data.moves.value;
    for (const move of data.startingMoves.value) {
      moves.push(move);
    }
    changes["data.moves.value"] = moves;
    this.actor.update(changes, true);
  }

  /**
   * Imports a move to this sheet.
   */
  importMove(move, pack) {
    const data = super.getData();
    const moves = data.data.moves.value;
    moves.push({name: move.name, id: move._id, pack: pack,
      description: move.data.description.value});
    this.actor.update({["data.moves.value"]: moves}, true);
  }
}

CONFIG.Actor.entityClass = DwActor;
CONFIG.Actor.sheetClass = DwActorSheet;
CONFIG.DwActorSheet = {
  "width": 720,
  "height": 800
};
