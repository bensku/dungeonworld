// Dungeon world item stuff
// Note: 'item' refers to everything that characters might have including classes

function parseList(str) {
  if (!str.includes(",") && !str.includes("and") && !str.includes("or")) {
    return [str]; // Regex gets weird when string cannot be split
  }
  return str.split(/, | and | or /);
}

function joinList(list, separator, lastSeparator) {
  if (list.length == 0 || list[0] === "") {
    return ""; // Nothing to show
  }
  if (list.length == 1) {
    return list[0]; // Only one element to show
  }
  let str = list[0];
  for (let i = 1; i < list.length - 1; i++) {
    str += separator + list[i];
  }
  str += lastSeparator + list[list.length - 1];
  return str;
}

class DwItemSheet extends ItemSheet {
  constructor(item, options) {
    super(item, options);
    if (item.data.type == "class") {
      this.options.width = 700;
    }
    this.mce = null;
  }

  getData() {
    const data = super.getData();

    // Class looks
    const looks = data.data.looks;
    if (looks != undefined) {
      for (let name in looks) {
        const look = looks[name];
        if (look.value != undefined) { // This is not the label
          look.text = joinList(look.value, ", ", " or ");
        }
      }
    }
    console.log(data);

    return data;
  }

  get template() {
    const type = this.item.type;
    return `public/systems/dungeonworld/templates/item-${type}-sheet.html`;
  }

  /**
   * After the template is rendered, this gets the HTML. Most interactivity is
   * added here.
   */
  activateListeners(html) {
    super.activateListeners(html);

    // Activate tabs
    html.find('.tabs').each((_, el) => {
      let tabs = $(el),
        initial = "overview";
      new Tabs(tabs, initial);
    });

	  // Activate TinyMCE editor buttons
	  html.find(".editor a.editor-edit").click(ev => {
	    let button = $(ev.currentTarget),
	        editor = button.siblings(".editor-content");
	    createEditor({
        target: editor[0],
        height: editor.parent().height() - 40,
        save_enablewhendirty: true,
        save_onsavecallback: ed => this._onSaveMCE(ed, editor.attr("data-edit"))
      }).then(ed => {
        this.mce = ed[0];
        button.hide();
        this.mce.focus();
      });
    });

    // Activate simple autosave editors
    html.find(".editable").change(ev => {
      let field = $(ev.currentTarget);
      let target = field.attr("data-edit");
      let data = field.val();
      if (target.endsWith("@list")) { // Parse into a list
        target = target.substring(0, target.length - 5);
        data = parseList(data);
      }
      let itemData = {[target]: data};
      this.updateItemData(itemData);
    });

    // Make the sheet droppable for other items
    // Foundry core only adds this for actor sheets
    const form = html[0];
    form.ondragover = ev => this._onDragOver(ev);
    form.ondrop = ev => this._onDrop(ev);

    // Universal remove buttons
    html.find(".remove-button").click(ev => {
      const button = $(ev.currentTarget);
      const id = parseInt(button.attr("remove-id"));
      const arrayPath = button.attr("remove-array");
      let array = super.getData();
      for (const part of arrayPath.split(".")) {
        array = array[part];
      }

      new Dialog({
        title: button.attr("remove-title"),
        content: button.attr("remove-content"),
        buttons: {
          requires: {
            label: "Yes",
            callback: () => {
              array.splice(id, 1);
              this.updateItemData({[arrayPath]: array});
            }
          },
          replaces: {
            label: "No",
            callback: () => {
              // Do nothing, user cancelled
            }
          }
        }}).render(true);
    });

    // Open links
    html.find(".dw-link").click(ev => {
      const button = $(ev.currentTarget);
      const id = parseInt(button.attr("open-id"));
      const pack = button.attr("open-pack");

      // TODO implement links
    });

    // Class editing functionality
    if (this.item.data.type == "class") {
      this._activateClassListeners(html);
    }
  }

  /**
   * Item type "class" specific listeners.
   */
  _activateClassListeners(html) {
    // Race add button
    html.find(".add-race").click(ev => {
      const races = super.getData().data.races.value;
      races.push({
        name: {},
        names: {},
        move: {}
      });
      this.updateItemData({["data.races.value"]: races});
    });

    // Race remove button
    html.find(".remove-race").click(ev => {
      const button = $(ev.currentTarget);
      const id = parseInt(button.attr("remove-id"));

      let races = super.getData().data.races.value;
      let name = races[id].name.value;

      new Dialog({
        title: `Delete race ${name}`,
        content: `Are you sure you want to remove race ${name} from this class?`,
        buttons: {
          requires: {
            label: "Yes",
            callback: () => {
              races.splice(id, 1);
              this.updateItemData({["data.races.value"]: races});
            }
          },
          replaces: {
            label: "No",
            callback: () => {
              // Do nothing, user cancelled
            }
          }
        }}).render(true);
    });

    // Alignment add button
    html.find(".add-alignment").click(ev => {
      const alignments = super.getData().data.alignments.value;
      alignments.push({
        label: "",
        value: ""
      });
      this.updateItemData({["data.alignments.value"]: alignments});
    });
  }

  /**
   * Customize sheet closing behavior to ensure we clean up the MCE editor.
   */
  close() {
    super.close();
    if (this.mce) {
      this.mce.remove();
      this.mce.destroy();
    }
  }

  _onSaveMCE(ed, target) {
    const form = this.element.find('.item-sheet')[0];
    const itemData = validateForm(form);
    itemData[target] = ed.getContent();
    this.updateItemData(itemData);

    // Destroy the editor
    ed.remove();
    ed.destroy();
  }

  /**
   * Called when this item needs updating. Handles both owned and unowned
   * items and makes sure name is properly updated everywhere.
   * @param itemData
   */
  updateItemData(itemData) {
    // Prepare name for update
    if (itemData["data.name"] != undefined) {
      itemData.name = itemData["data.name"];
    }

    // Update owned items
    if (this.item.isOwned) {
      itemData.id = this.item.data.id;
      this.item.actor.updateOwnedItem(itemData, true);
      this.render(false);
    }

    // Update unowned items
    else {
      console.log("update:");
      console.log(this.item.update);
      this.item.update(itemData, true);
      this.render(false);
    }
  }

  /**
   * Called when something is dragged over this window.
   * @param event {Event}
   * @return {Boolean} False.
   * @private
   */
  _onDragOver(event) {
    event.preventDefault();
    return false;
  }

  /**
   * Bring something from a compendium.
   * @param collection {String}     The name of the pack from which to import
   * @param entryId {String}        The ID of the compendium entry to import
   *
   * @return {Promise}              A Promise containing the imported entity
   */
  _getFromCompendium(collection, entryId) {
    const pack = game.packs.find(p => p.collection === collection);

    return pack.getEntity(entryId).then(ent => {
      delete ent.data._id;
      return ent.constructor.create(ent.data, true);
    });
  }

  /**
   * Called when something is dropped on this window.
   * @param  event {Event}
   * @return {Boolean} False.
   * @private
   */
  _onDrop(event) {
    event.preventDefault();
    const data = JSON.parse(event.dataTransfer.getData('text/plain'));
    let entry;
    if (data.pack) { // From compendium
      entry = this._getFromCompendium(data.pack, data.id).data;
    } else if ( data.type === "Item" ) { // From world
      entry = new Promise((resolve, reject) => {
        resolve(game.items.get(data.id).data);
      });
    }

    // Actually handle the data
    entry.then((entity) => {
      if (this.item.data.type == "move") {
        this._addMoveToMove(entity, data.pack);
      } else if (this.item.data.type == "class") {
        this._addMoveToClass(entity, data.pack);
      }
    });

    return false;
  }

  /**
   * Asks the user whether this move replaces given move, or merely requires it.
   * Then, depending on choice, modifies data of this item.
   * @private
   */
  _addMoveToMove(move, pack) {
    const data = super.getData(); // We don't need additional processing for templates

    // Create a dialog, whose buttons trigger actual additions
    let array;
    let itemData;
    new Dialog({
      title: "Add Move",
      content: `How do you want move ${data.item.name} to depend on ${move.name}?`,
      buttons: {
        requires: {
          label: "Requires",
          callback: () => {
            array = data.data.requires.value;
            itemData = {["data.requires.value"]: array};
          }
        },
        replaces: {
          label: "Replaces",
          callback: () => {
            array = data.data.replaces.value;
            itemData = {["data.replaces.value"]: array};
          }
        }
      },
      close: html => {
        array.push({name: move.name, id: move._id, pack: pack});
        this.updateItemData(itemData);
      }
    }).render(true);
  }

  /**
   * Asks the user how to add this move to class. Choices:
   * 1) Starting move
   * 2) Advanced move, level 1-5
   * 3) Advanced move, level 6-10
   * @private
   */
  _addMoveToClass(move, pack) {
    const data = super.getData(); // We don't need additional processing for templates

    // Create a dialog, whose buttons trigger actual additions
    let array;
    let itemData;
    console.log(move);
    new Dialog({
      title: "Add Move",
      content: `How do you want move ${move.name} to be added to ${data.item.name}?`,
      buttons: {
        starting: {
          label: "Starting",
          callback: () => {
            array = data.data.startingMoves.value;
            itemData = {["data.startingMoves.value"]: array};
          }
        },
        advanced1: {
          label: "Advanced (level 2-5)",
          callback: () => {
            array = data.data.advancedMoves1.value;
            itemData = {["data.advancedMoves1.value"]: array};
          }
        },
        advanced2: {
          label: "Advanced (level 6-10)",
          callback: () => {
            array = data.data.advancedMoves2.value;
            itemData = {["data.advancedMoves2.value"]: array};
          }
        }
      },
      close: html => {
        array.push({name: move.name, id: move._id, pack: pack,
          description: move.data.description.value,
          requires: move.data.requires.value, replaces: move.data.replaces.value});
        this.updateItemData(itemData);
      }
    }).render(true);
  }
}

CONFIG.Item.sheetClass = DwItemSheet;
